# Nambari

Hii ni mchezo ya kusomesha watoto kuhesabu

## Kutengeneza kwa Fedora Linux

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo dnf -y groupinstall "Development Tools"
sudo dnf -y install gcc SDL2-devel SDL2_image-devel SDL2_ttf-devel espeak-ng-devel
bash compile.sh
./SwahiliNumbers
```

## Kutengeneza kwenye Ubuntu | Debian

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo apt install build-essential
sudo apt install gcc libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libespeak-ng-dev
bash compile.sh
./SwahiliNumbers
```

## Fonti

Kutoka https://github.com/tonsky/FiraCode  OFL-1.1


## Picha

![](matatus/Matatu_Manyanga.jpg)

https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg  CC-BY-SA 4.0

![](matatus/A_matatu.jpg)

https://commons.wikimedia.org/wiki/File:A_matatu.jpg CC-BY-SA 4.0

![](matatus/MATATU_MATWANA_CULTURE_KENYA_04.jpg)

https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg CC-BY-SA 4.0

![](matatus/Matatu4.jpg)

https://commons.wikimedia.org/wiki/File:Matatu4.jpg  CC-BY-SA 4.0

![](matatus/Matatus_Kampala.jpg)

https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg CC-BY-SA 3.0

![](matatus/MATATU_MATWANA_CULTURE_KENYA_04.jpg)

https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg CC-BY-SA 4.0

![](matatus/Blue_Matatu_with_colourful_graphiti.jpg)

https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg CC-BY-SA 4.0

## Jisomesha SDL2

- https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/

- https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/

- https://wiki.libsdl.org/FrontPage

# Numbers

A game to teach children how to count

## To compile on Fedora Linux

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo dnf -y groupinstall "Development Tools"
sudo dnf -y install gcc SDL2-devel SDL2_image-devel SDL2_ttf-devel espeak-ng-devel
bash compile.sh
./SwahiliNumbers
```

## To compile on Ubuntu | Debain

```sh
git clone https://gitlab.com/nairuby-games/nambari
cd nambari
sudo apt install build-essential
sudo apt install gcc libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libespeak-ng-dev
bash compile.sh
./SwahiliNumbers
```

## Fonts

From https://github.com/tonsky/FiraCode  OFL-1.1

## Pictures

![](matatus/Matatu_Manyanga.jpg)

https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg  CC-BY-SA 4.0

![](matatus/A_matatu.jpg)

https://commons.wikimedia.org/wiki/File:A_matatu.jpg CC-BY-SA 4.0

![](matatus/MATATU_MATWANA_CULTURE_KENYA_04.jpg)

https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg CC-BY-SA 4.0

![](matatus/Matatu4.jpg)

https://commons.wikimedia.org/wiki/File:Matatu4.jpg  CC-BY-SA 4.0

![](matatus/Matatus_Kampala.jpg)

https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg CC-BY-SA 3.0

![](matatus/MATATU_MATWANA_CULTURE_KENYA_04.jpg)

https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg CC-BY-SA 4.0

![](matatus/Blue_Matatu_with_colourful_graphiti.jpg)

https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg CC-BY-SA 4.0

## Teach yourself SDL2

- https://www.geeksforgeeks.org/sdl-library-in-c-c-with-examples/

- https://gigi.nullneuron.net/gigilabs/writing/sdl2-tutorials/

- https://wiki.libsdl.org/FrontPage
