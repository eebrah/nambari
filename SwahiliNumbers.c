#include <espeak-ng/speak_lib.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_ttf.h>

espeak_AUDIO_OUTPUT output = AUDIO_OUTPUT_SYNCH_PLAYBACK;
char *path = NULL;
void* user_data;
unsigned int *identifier;
 
int main(int argc, char* argv[]) {
 char voicename[] = {"English"}; // Set voice by its name
 char *text[15] = {"Moja","Mbili","Tatu","Nne","Tano","Sita",
	   "Saba","Nane","Tisa","Kumi","Sahihi","Batili",
	   "Umejaribu","Ndugu, matatu ngapi?","Hesabu"};
 int buflength = 500, options = 0, i = 2, correct=0, incorrect=0, 
       questions = 5, num=0, texttexW=0, texttexH=0;
 int done = 0, close = 0, mouseX = 0, mouseY = 0, mousedown = 0;
 time_t seconds;
 time(&seconds);
 srand(seconds);
 unsigned int position = 0, position_type = 0, end_position = 0, flags = espeakCHARS_AUTO;
           espeak_Initialize(output, buflength, path, options);
           espeak_SetVoiceByName(voicename);
 // returns zero on success else non-zero
 if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
  printf("Makosa SDL ikianza: %s\n", SDL_GetError());
 }
 // Initialize Font library
 TTF_Init();
 // creates a window
 SDL_Window* win = SDL_CreateWindow("Matatu ngapi?",
                        SDL_WINDOWPOS_CENTERED,
                        SDL_WINDOWPOS_CENTERED,
	  	 500, 500, 0);
 // triggers the program that controls
 // your graphics hardware and sets flags
 Uint32 render_flags = SDL_RENDERER_ACCELERATED;
 // creates a renderer to render our images
 SDL_Renderer* rend = SDL_CreateRenderer(win, -1, render_flags);
 // creates a surface to load an image into the main memory
 SDL_Surface* surface[11];
 // Graphics hardware memory for surface
 SDL_Texture* tex[11];
 // Control image position
 SDL_Rect dest[11];
 // Squares for input grid
 SDL_Rect rectgrid[10];
 // Surface for input grid
 SDL_Surface* gridsurface[10];
 // Texture for input grid
 SDL_Texture* gridtexture[10];
 // Event variable
 SDL_Event event;
 for(int k=0;k<10;k++)
 {
  // provide path to image
  surface[k] = IMG_Load("matatus/MatatuCounter2.png");
  // loads image to our graphics hardware memory
  tex[k] = SDL_CreateTextureFromSurface(rend, surface[k]);
  // clears main-memory
  SDL_FreeSurface(surface[k]);
 }
 // Load cover image
 surface[10] = IMG_Load("matatus/Cover.png");
 // loads image to our graphics hardware memory
 tex[10] = SDL_CreateTextureFromSurface(rend, surface[10]);
 // clears main-memory
 SDL_FreeSurface(surface[10]);
 // Load font
 TTF_Font* font = TTF_OpenFont("FiraCode-Medium.ttf",30);
 // Choose font color
 SDL_Color color = { 255, 255, 255 };
 SDL_Surface* textsurface = TTF_RenderText_Solid(font,
			text[13], color);
 SDL_Texture* texttexture = SDL_CreateTextureFromSurface(
			rend, textsurface);
 // Hello sisters son count cars
 SDL_QueryTexture(texttexture, NULL, NULL, &texttexW, &texttexH);
 SDL_Rect dstrect = { 0, 0, texttexW, texttexH };
 SDL_RenderCopy(rend,texttexture, NULL, &dstrect);
 // Add cover image
 // control position
 SDL_QueryTexture(tex[10], NULL, NULL,
                 &dest[10].w, &dest[10].h);
 // adjust height and width of our image
 dest[10].w /= 6;
 dest[10].h /= 6;
 // sets initial x-position of object
 dest[10].x = (500 - dest[10].w) / 2;
 // sets initial y-position of object
 dest[10].y = (500 - dest[10].h) / 2;
 // Add image
 SDL_RenderCopy(rend, tex[10], NULL, &dest[10]);
 SDL_RenderPresent(rend);
 espeak_Synth(text[13], buflength, position, position_type,
                        end_position, flags, identifier, user_data);
 // Pause
 SDL_Delay( 5000 );
 // Clear screen
 SDL_RenderClear(rend);
 // question loop
 for(int j=0;j<questions;j++)
 {
  i=1+rand()%10;
  textsurface = TTF_RenderText_Solid(font, "Jumlisha", color);
  texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
  SDL_QueryTexture(texttexture, NULL, NULL, &texttexW, &texttexH);
  dstrect.w = texttexW; 
  dstrect.h = texttexH;
  for(int k=0;k<i;k++)
  {
   // connects our texture with dest to 
   // control position
   SDL_QueryTexture(tex[k], NULL, NULL, 
			&dest[k].w, &dest[k].h);
   // adjust height and width of our image
   dest[k].w /= 6;
   dest[k].h /= 6;
   // sets initial x-position of object
   dest[k].x = (100 + (k%5)*190 - dest[k].w) / 2 ;
   // sets initial y-position of object
   dest[k].y = (200 + ((k-(k%5))/5)*190 - dest[k].h) / 2;
  }
  // clears the screen
  SDL_RenderClear(rend);
  // write text
  SDL_RenderCopy(rend,texttexture, NULL, &dstrect);
  for(int k=0;k<i;k++)
  {
   SDL_RenderCopy(rend, tex[k], NULL, &dest[k]);
  }
  // Make grid press points
  for(int k=0;k<10;k++)
  {
   // Red background	  
   rectgrid[k].h = 90;
   rectgrid[k].w = 90;
   rectgrid[k].x = 5 + 100 * (k%5);
   rectgrid[k].y = 305 + 100 * (k/5) ;
   gridsurface[k] = SDL_CreateRGBSurface(0, rectgrid[k].w, rectgrid[k].h, 32, 0, 0, 0, 0); 
   SDL_FillRect(gridsurface[k], NULL, 
  		  SDL_MapRGB(gridsurface[k]->format, 255, 0, 0));
   gridtexture[k] = SDL_CreateTextureFromSurface(rend, gridsurface[k]);
   SDL_RenderCopy(rend,gridtexture[k], NULL, &rectgrid[k]);
   // White text
   rectgrid[k].x+=20;
   rectgrid[k].y+=27;
   if(k==9) rectgrid[k].x-=9;
   char numout[4];
   sprintf(numout, " %d", k+1);
   textsurface = TTF_RenderText_Solid(font,numout,color);
   texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
   SDL_QueryTexture(texttexture, NULL, NULL, &rectgrid[k].w, &rectgrid[k].h);
   SDL_RenderCopy(rend,texttexture, NULL, &rectgrid[k]);
  }
  // triggers the double buffers
  // for multiple rendering
  SDL_RenderPresent(rend);
  espeak_Synth(text[i-1], buflength, position, 
		   position_type, end_position, 
		   flags, identifier, user_data);
  done = 0;
  mousedown = 0;
  // Events management
  while(!done)
  {
   while(SDL_PollEvent(&event))
   {
    switch (event.type)
    {
     case SDL_QUIT:
      // handling of close button
      done = 1;
      close = 1;
      break;
     case SDL_MOUSEBUTTONDOWN:
      switch (event.button.button)
      {
       case SDL_BUTTON_LEFT:
	num = floor( mouseX / 100.0 ) + 1 + 5 * floor( ( mouseY - 300.0 ) / 100.0) ;
	mousedown=1;
	done=1;
	break;
       case SDL_BUTTON_RIGHT:
	num = floor( mouseX / 100.0 ) + 1 + 5 * floor( ( mouseY - 300.0 ) / 100.0) ;
        mousedown=1;
	done=1;
	break;
      }	
     case SDL_MOUSEMOTION:
      mouseX = event.motion.x;
      mouseY = event.motion.y; 
     case SDL_KEYDOWN:
      // keyboard API for key pressed
      switch (event.key.keysym.scancode)
      {
       default:
        num = num;
      } // end switch (event.key.keysym.scancode)
    } // end switch (event.type)
    // check for events to 60 time per second
    SDL_Delay(1000 / 60 );
   } // end while(SDL_PollEvent(&event))
  } // end while(!done)
  //exit if window is closed
  if(close) break;
  // Checking keypress
  if(done)
  {
   // Checking answer	   
   if(num==i)
   {
    // Print "Sahihi" and update correct count
    SDL_RenderClear(rend);
    textsurface = TTF_RenderText_Solid(font, "Sahihi", color);
    texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
    SDL_QueryTexture(texttexture, NULL, NULL, &texttexW, &texttexH);
    dstrect.w = texttexW; 
    dstrect.h = texttexH;
    SDL_RenderCopy(rend,texttexture, NULL, &dstrect);
    SDL_RenderPresent(rend);
    espeak_Synth(text[10], buflength, position, 
		   position_type, end_position, 
		   flags, identifier, user_data);
    correct++;
    // reset value
    num = 0;
    // Pause
    SDL_Delay( 2000 );
    // animation
    for(int kk=0;kk<500;kk++)
    {
     for(int k=0;k<i;k++)
     {
      // sets  x-position of object
      dest[k].x = (100 + kk*2+ (k%5)*190 - dest[k].w) / 2 ;
      // sets y-position of object
      dest[k].y = (200 + ((k-(k%5))/5)*190 - dest[k].h) / 2;
     }
     // clears the screen
     SDL_RenderClear(rend);
     for(int k=0;k<i;k++)
     {
      SDL_RenderCopy(rend, tex[k], NULL, &dest[k]);
     }
     // triggers the double buffers
     // for multiple rendering
     SDL_RenderPresent(rend);
     SDL_Delay( 1000 / 60 );
    }
    for(int k=0;k<i;k++)
    {
     // resets initial x-position of object
     dest[k].x = (100 + (k%5)*190 - dest[k].w) / 2 ;
     // resets initial y-position of object
     dest[k].y = (200 + ((k-(k%5))/5)*190 - dest[k].h) / 2;
    }
   }else{
    // Print "Batili" and update incorrect count
    SDL_RenderClear(rend);
    textsurface = TTF_RenderText_Solid(font,"Batili", color);
    texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
    SDL_QueryTexture(texttexture, NULL, NULL, &texttexW, &texttexH);
    dstrect.w = texttexW; 
    dstrect.h = texttexH;
    SDL_RenderCopy(rend,texttexture, NULL, &dstrect);
    SDL_RenderPresent(rend);
    espeak_Synth(text[11], buflength, position, 
		position_type, end_position, 
		flags, identifier, user_data);
    incorrect++;
    // reset value
    num = 0;
    // Pause
    SDL_Delay( 5000 );
   } // end if (num==i)
  } // end if if(event.type==SDL_KEYDOWN)
 } // end for(int j=0;j<questions;j++)
 if(!close)
 {
  // Print number of correct answers
  char lastmessage[100];
  char numout[10];
  sprintf(lastmessage, "%s", text[12]);
  sprintf(numout, " %d", correct);
  strcat(lastmessage, numout);
  strcat(lastmessage, " / ");
  sprintf(numout, "%d", questions);
  strcat(lastmessage, numout);
  SDL_RenderClear(rend);
  textsurface = TTF_RenderText_Solid(font, lastmessage, color);
  texttexture = SDL_CreateTextureFromSurface(rend, textsurface);
  SDL_QueryTexture(texttexture, NULL, NULL, &texttexW, &texttexH);
  dstrect.w = texttexW; 
  dstrect.h = texttexH;
  SDL_RenderCopy(rend,texttexture, NULL, &dstrect);
  SDL_RenderPresent(rend);
  espeak_Synth(text[12], buflength, position,
                   position_type, end_position,
                   flags, identifier, user_data);
  // pause
  SDL_Delay( 5000 );
 }
 // destroy textures
 for(int k=0;k<10;k++)
 {
  SDL_DestroyTexture(tex[k]);
  SDL_DestroyTexture(gridtexture[k]);
  SDL_FreeSurface(gridsurface[k]);
 }
 SDL_DestroyTexture(texttexture);
 SDL_FreeSurface(textsurface);
 // Close Font
 TTF_CloseFont(font);
 // destroy renderer
 SDL_DestroyWindow(win);
 // cleanup font library
 TTF_Quit();
 // close SDL
 SDL_Quit();
 return 0;
}
