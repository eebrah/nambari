# Picture Sources and permissions


|Source | Licence |
|---|---|
| https://commons.wikimedia.org/wiki/File:Matatu_Manyanga.jpg |  CC-BY-SA 4.0 |
| https://commons.wikimedia.org/wiki/File:A_matatu.jpg | CC-BY-SA 4.0 |
| https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg | CC-BY-SA 4.0 |
| https://commons.wikimedia.org/wiki/File:Matatu4.jpg | CC-BY-SA 4.0 |
| https://commons.wikimedia.org/wiki/File:Matatus_Kampala.jpg | CC-BY-SA 3.0 |
| https://commons.wikimedia.org/wiki/File:MATATU_MATWANA_CULTURE_KENYA_04.jpg | CC-BY-SA 4.0 |
| https://commons.wikimedia.org/wiki/File:Blue_Matatu_with_colourful_graphiti.jpg | CC-BY-SA 4.0 |
